
<!DOCTYPE html>
<html>

<head>
	<meta charset='UTF-8' />
	
<link rel="stylesheet" type="text/css" href="css/main.css" />
<link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="header">
	<img src="binaries/logo.png" style="float:right;margin-top:10px;margin-right:30px;" />
	<div id="nav">
		<a href="#about">¿ por qu&eacute; ?</a> | <a href="#portfolio">portafolio</a> | <a href="#equipo">equipo</a> | <a href="#contact">contacto</a> | ll&aacute;manos al +(52) 55 4164 9555
	</div>
</div>
<div id="page-wrap">
	<div id="page1" class="sombra">
		<a name="about"></a>
		<p><span class="resalta">Aplicaciones en L&iacute;nea</span> es un equipo de trabajo que realiza proyectos de desarrollo con un alto enfoque en usabilidad y flujos de uso.</p>
		<p>
		<span class="resalta">Lo que hacemos:</span>
			<ul>
				<li>Desarrollamos Front-Ends para Plataformas Web.</li>
				<li>Realizamos benchmarking y optimizamos tus sitios Web para máximo rendimiento y exposici&oacute;n.</li>
			</ul>
		</p>
		<p>
		<span class="resalta">Lo que usamos:</span>
			<ul>
				<li>Git / BitBucket / GitHub / Deploy HQ</li>
				<li>Asana / Let's Freckle</li>
				<li>PHP / Ruby / Java / Javascript</li>
				<li>Sammy.js / doT.js / jQuery / Mustache.js / Yii Framework / Ruby on Rails / jQuery / Titanium / PhoneGap </li>
			</ul>
		</p>
	</div> <!--END page1-->
	<div id="page2">
		<a name="portfolio"></a><h1>portafolio</h1>
		<div style="background-image: url('binaries/portafolio.jpg');height:2600px;"></div>
	</div> <!--END page2-->

	<div id="page3" class="sombra" style="clear:both;">
		<a name="equipo"></a><h1>equipo</h1>
		<img src="binaries/nosotros/s.jpg" style="float:left;border:solid 5px white;margin-right:20px;margin-left:20px;">
		<p style="font-weight:bolder;">Salvador Aceves</p>
		<p>Developer desde que tiene uso de raz&oacute;n, con un fuerte enfoque en Mercadotecnia Digital.</p>
		<p><a href="http://mx.linkedin.com/in/salvadoraceves">Linked-In</a> | <a href="http://about.me/salvadoraceves">About.me</a></p>
		<img src="binaries/nosotros/a.jpg" style="float:left;border:solid 5px white;margin-right:20px;margin-left:20px;">
		<p style="font-weight:bolder;">Alejandro G&oacute;mez</p>
		<p>Desarrollador orientado a web, gusta de maquetar y procurar buenas pr&aacute;cticas de programaci&oacute;n. Met&oacute;dico.</p>
		<p><a href="http://mx.linkedin.com/pub/alejandro-gomez/47/533/a05">Linked-In</a></p>
		<img src="binaries/nosotros/c.jpg" style="float:left;border:solid 5px white;margin-right:20px;margin-left:20px;">
		<p style="font-weight:bolder;">Carlos Flores</p>
		<p>Desarrollador especializado en frontend, gusta de desarrollador herramientas para si mismo, Chef de C&oacute;digo.</p>
		<p><a href="http://mx.linkedin.com/in/carlosfloresbenavides">Linked-In</a></p>
	</div> <!--END page3-->

	<div id="page4" class="sombra">
		<a name="contact"></a><h1>contacto</h1>
		<iframe style="margin-left:70px;float:left;" src="https://docs.google.com/spreadsheet/embeddedform?formkey=dFplNHZoTnBIN2dRMENycEZDZE1KeXc6MQ" width="400" height="578" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
		<iframe style="margin-left:70px;float:left;" width="400" height="578" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=Tankah+74,+Canc%C3%BAn,+QROO,+M%C3%A9xico&amp;aq=0&amp;oq=Tankah+&amp;sll=37.0625,-95.677068&amp;sspn=41.632176,78.662109&amp;ie=UTF8&amp;hq=&amp;hnear=Tankah+74,+Canc%C3%BAn,+Quintana+Roo,+M%C3%A9xico&amp;t=m&amp;ll=21.164003,-86.834736&amp;spn=0.044023,0.034246&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
		<div class="page-padding"></div>
		<p></p>
	</div> <!--END page3-->
	
</div> <!--END page-wrap-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script>$(document).ready(function() {
  function filterPath(string) {
    return string
      .replace(/^\//,'')  
      .replace(/(index|default).[a-zA-Z]{3,4}$/,'')  
      .replace(/\/$/,'');
  }
  $('a[href*=#]').each(function() {
    if ( filterPath(location.pathname) == filterPath(this.pathname)
    && location.hostname == this.hostname
    && this.hash.replace(/#/,'') ) {
      var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) +']');
      var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
       if ($target) {
         var targetOffset = $target.offset().top;
         $(this).click(function() {
           $('html, body').animate({scrollTop: targetOffset-70}, 2000);
           return false;
         });
      }
    }
  });
});</script>
</body>
</html>